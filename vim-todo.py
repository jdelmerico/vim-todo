# General Imports
import httplib2
import cPickle as pickle
import shutil
import sys
import os
import difflib
import datetime

# Google Tasks specific imports
from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.tools import run


def getTaskId(task,taskIds):
    if task['id'] in taskIds:
        tid = taskIds[task['id']]
    else:
        # Increase the 'max' value by 1
        # or add it if it doesn't exist yet
        if 'max' in taskIds:
            taskIds['max'] = taskIds['max'] + 1
        else:
            taskIds['max'] = 1
        # Add this key to the dictionary
        taskIds[taskIds['max']] = task['id']
        # both ways
        taskIds[task['id']] = taskIds['max']
        tid = taskIds['max']
    return tid

def formatTask(task,depth,tid):
    fmt = ''
    # First indent for depth of nesting
    fmt = fmt + depth*'    '
    # Then do checkbox
    if task['status'] == 'completed':
        fmt = fmt + '[X] '
    else:
        fmt = fmt + '[_] '
    # Then the due date
    if 'due' in task:
        fullDueDate = str(task['due'])
        fmt = fmt + '(' + fullDueDate[:10] + ') '
    # Then the task title itself
    fmt = fmt + task['title']
    # Print the TaskId
    fmt = fmt + ' {' + str(tid) + '}'
    # Then the notes if there are any
    if 'notes' in task:
        notes = task['notes'].replace('\n','|')
        fmt = fmt + ' | ' + notes
    return fmt
        
def tasksInList(service,listID,taskStrings,taskIds):
    tasks = service.tasks().list(tasklist=listID).execute()
    try: 
        for task in tasks['items']:
            if task['title'] == '': 
                pass
            else:
                # Determine nesting level
                depth = 1
                tmpTask = task 
                while 'parent' in tmpTask.keys():
                    depth = depth + 1
                    tmpTask = service.tasks().get(tasklist=listID, task=tmpTask['parent']).execute() 
                # Get local task id #
                tid = getTaskId(task,taskIds) 
                taskStrings.append(formatTask(task,depth,tid))
    except KeyError: pass 

def getTasks(service,tasklists,listIds,taskIds):
    taskStrings = []
    for tasklist in tasklists['items']:
        taskStrings.append(tasklist['title'])
        listID=tasklist['id']
        listTitle=tasklist['title']
        listIds[listTitle] = listID
        tasksInList(service,listID,taskStrings,taskIds)					
        taskStrings.append('')
    return taskStrings

def nestingDepth(line):
    spaces = len(line) - len(line.lstrip(' '))
    tabs = len(line) - len(line.lstrip('\t'))
    return spaces/4 + tabs

def extractDiffChanges(diff):
    adds = []
    dels = []
    chngs = []
    # Iterate through lines in diff, pull out changes to appropriate list
    dl = list(diff)
    listName = ''
    for i in range(0,len(dl)):
        l = dl[i]
        # Check depth
        depth = nestingDepth(l) 
        if l[2] != ' ' and l[2] != '\t':
            # This is a list that already existed
            listName = l[2:].rstrip()
        if l[0] == '+' and l[1] == ' ':
            if l[2] != ' ' and l[2] != '\t':
                # We're adding a new list
                adds.append( (listName,None) )
            elif i == len(dl)-1:
                # Last line in file was added
                adds.append( (listName,l[2:].rstrip()) )
            elif dl[i+1][0] == '?':
                # This line is modified
                chngs.append( (listName,l[2:].rstrip()) )
            else:
                # This is a new line
                adds.append( (listName,l[2:].rstrip()) )
        if l[0] == '-' and l[1] == ' ':
            if l[2] != ' ' and l[2] != '\t':
                # We're adding a new list
                dels.append( (listName,None) )
            elif i == len(dl)-1:
                # Last line in file was deleted
                dels.append( (listName,l[2:].rstrip()) )
            elif dl[i+1][0] == '?':
                # This line was modified to something else
                pass
            else:
                # This line was deleted
                dels.append( (listName,l[2:].rstrip()) )
    return (adds,dels,chngs)

def parseTaskText(text):
    # Accepted Format:
    # [_/X] (yyyy-mm-dd) Title {id} | notes
    # Return is (status,due,title,id,notes)
    status = None
    due = None
    title = None
    tid = None
    notes = None
    rtn = (status,due,title,tid,notes)
    if text[0] != '[' or text[2] != ']':
        print "Poorly formed checkbox:\n%s"%text
        return rtn
    if text[1] == '_':
        status = 'needsAction'
        text = text[3:]
    elif text[1] == 'X':
        status = 'completed'
        text = text[3:]
    else:
        print "Poorly formed checkbox:\n%s"%text
        return rtn
    if text[1] == '(' and text[12] == ')':
        dateString = text[2:11]
        try:
            y,m,d = dateString.split('-')
            date = datetime.date(int(y),int(m),int(d))
            due = date.isoformat() + "T12:00:00.000Z"
            text = text[13:]
        except ValueError:
            print "Poorly formed date:\n%s"%text
    titleNotes = text.split('|')
    if len(titleNotes) > 1:
        notes = titleNotes[1]
    titleId = titleNotes[0].split('{')
    if len(titleId) > 1:
        idString = titleId[1]
        if idString[-2] == '}' and idString[-1] == ' ':
            tid = idString[:-3] 
    title = titleId[0]
    rtn = (status,due,title,tid,notes)
    return rtn

def generateTaskQuery(taskText):
    # determine nesting depth - unused until parent task sorted out
    depth = nestingDepth(taskText)
    # Parse all other supported fields
    status,due,title,tid,notes = parseTaskText(taskText.lstrip())
    tq = {}
    # Add keys to query
    if title is not None:
        tq['title'] = title
    else:
        # This task could not be parsed correctly
        return None
    if status is not None:
        tq['status'] = status
    else:
        # This task could not be parsed correctly
        return None
    if due is not None:
        tq['due'] = due
    if tid is not None:
        tq['id'] = tid
    if notes is not None:
        tq['notes'] = notes
    return tq

def addNewTasks(service, adds, listIds, taskIds):
    for l, t in adds:
        # Add any new lists first
        if t is None:
            query = {'title':l}
            result = service.tasklists().insert(body=query).execute()
            # add new list id to local db
            listIds[l] = result['id']
    for l, t in adds:
        # Then add any new tasks
        if t is not None:
            query = generateTaskQuery(t)
            lid = listIds[l]
            # Swap out local id for Google id
            query['id'] = getTaskId(query['id'],taskIds)
            result = service.tasks().insert(tasklist=lid, body=query).execute()
            # add new task id to local db
            getTaskId(result,taskIds)

def usage():
    print "\nvim-todo.py usage:\n"
    print ">> vim-todo /PATH/TO/TODO.otl\n"
    sys.exit(-1)

def main():
    # Handle command line argument 
    if len(sys.argv) != 2:
        usage()
    # Try to open todo list file
    todoFname = sys.argv[1]
    try:
        todoFile = open(todoFname,'r')
    except IOError:
        print 'File does not exist.\n'
        usage()
    # Read lines into list
    try:
        todoText = todoFile.readlines()
    finally:
        todoFile.close()

    # Set up a Flow object to be used if we need to authenticate. This
    # sample uses OAuth 2.0, and we set up the OAuth2WebServerFlow with
    # the information it needs to authenticate. Note that it is called
    # the Web Server Flow, but it can also handle the flow for native
    # applications
    # The client_id and client_secret are copied from the API Access tab on
    # the Google APIs Console
    FLOW = OAuth2WebServerFlow(
        client_id='742088272568-32m87oesllqfv1tdph7bm38d92mts67m.apps.googleusercontent.com',
        client_secret='v_i0bih-wXxJOHYEzs6Op15X',
        scope='https://www.googleapis.com/auth/tasks',
        user_agent='vim-todo/v0.1')

    # If the Credentials don't exist or are invalid, run through the native client
    # flow. The Storage object will ensure that if successful the good
    # Credentials will get written back to a file.
    storage = Storage('.taskCreds.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid == True:
      credentials = run(FLOW, storage)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Build a service object for interacting with the API. Visit
    # the Google APIs Console
    # to get a developerKey for your own application.
    service = build(serviceName='tasks', version='v1', http=http,
           developerKey='AIzaSyCidvwQ_hPsUlTALZZrbpVcIidvlIzLujQ')

    # Pull tasks
    tasklists = service.tasklists().list().execute()

    # Try to load pickle of dictionary with task Ids
    try:
        taskIds = pickle.load( open( ".taskIds.p", "rb" ))
    except:
        taskIds = {}

    # Try to load pickle of dictionary with list Ids
    try:
        listIds = pickle.load( open( ".listIds.p", "rb" ))
    except:
        listIds = {}
        
    # Compute deltas since last execution
    # Try to open old todo list file
    oldFname = ".%s.old"%os.path.basename(todoFname)
    oldText = []
    try:
        oldFile = open(oldFname,'r')
    except IOError:
        pass
    else:
        # Read lines into list
        try:
            oldText = oldFile.readlines()
        finally:
            oldFile.close()
    # Get diff of both sets of lines
    d = difflib.Differ()
    diff = d.compare(oldText,todoText)
    localAdds,localDels,localChngs = extractDiffChanges(diff)

    # Extract tasks from output from Google
    googleTasks = getTasks(service,tasklists,listIds,taskIds)

    # Push any new local tasks
    addNewTasks(service, localAdds, listIds, taskIds)
    
    # If deletions in deltas, remove these at Google
    
    # For changes, see if the task updated time is older than last push,
    # and keep Google changes if newer, else push changes

    # Pull all google tasks again and write out to original file name 

    # Cache all list and task ids
    pickle.dump( taskIds, open( ".taskIds.p", "wb" ))
    pickle.dump( listIds, open( ".listIds.p", "wb" ))

    # Copy todo file as it exists on exit so it can be compared on next run
    shutil.copy(todoFname,oldFname)

if __name__ == "__main__":
    main()
