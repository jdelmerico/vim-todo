# Overview #

This utility was designed to allow synchronization between a text file containing a ToDo list on the user's computer and the user's Google Tasks.  The syntax of the text file is simple and compatible with the style of checklists created with vimoutliner.  However, the user need not use a particular editor.  

# Dependencies #

Both of these can be installed using python easy_install:

* [Google Tasks Python API](https://github.com/google/google-api-python-client)
* [Google OAuth2 Client](https://github.com/google/oauth2client)

If you use vim as a text editor, the vimoutliner plugin will make the syntax we use convenient for handling ToDo list files.  Vim can be installed with apt-get on Linux, and the vimoutliner plugin can be downloaded [here](http://www.vim.org/scripts/script.php?script_id=3515).

## Usage ##

To synchronize a local ToDo list file with your Google Tasks, you can run:

```
python vim-todo.py /PATH/TO/TODO.otl
```
This will pull all of the tasks from your Google account, merge them into the local file, and then push all of the tasks in the merged file up to Google's servers.  Changes made either through the web or app interfaces, or in the text file, should be synchronized between the account and the file.

The first time this script is run, the user will need to authenticate the access to their Google tasks.  The user will be prompted to follow an html link to a Google consent screen, and will need to transfer a key from the following page to an input dialogue in the terminal. Thereafter, the program will reference those credentials, which will be stored locally in tasks.dat. This is working on the assumption that only one user will be sync-ing with only one Google account.

### TODO ###

* At end of program, copy the todo file to a local .old copy
* When beginning to run, use difflib (differ or context_diff) to get local deltas since last push
* Merging with existing file
    
    * Start by pulling the Google tasks, generate local ids for any that need them
    * then create a dictionary of task objects (keyed by local id)
    * Definitely add any new tasks that are in local deltas. Add these tasks up to Google and then add id to local db
    * If any tasks are in the google tasks but have been deleted in deltas, remove these at Google
    * For all items that are changed, see if the task updated time is older than last push, and keep Google changes if newer, else push changes
    * Pull all google tasks again and write out to original file name 

* Move old todo to .old copy and print all task lists to todo 
* set up chron job and maybe inotifywait script to run this whenever file is modified
* figure out how best to deal with parent tasks when adding new tasks

### Contact Info ###

Inquiries with feedback or requests for bug fixes can be directed to:
Jeff Delmerico (jeffdelmerico@gmail.com)
